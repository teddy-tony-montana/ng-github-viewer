import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RepositoryState } from '../../states/repository.state';
import { Select, Store } from '@ngxs/store';
import { Repository } from '../../models/Repository';
import { Observable } from 'rxjs';
import { GetRepository } from 'src/app/actions/repository.action';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Select(RepositoryState.getRepository) repository: Observable<Repository>;

  constructor(
    private store: Store,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.dispatch(new GetRepository(params['owner'], params['repo']));
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommitState } from '../../states/commit.state';
import { Select, Store } from '@ngxs/store';
import { Commit } from '../../models/Commit';
import { Observable } from 'rxjs';
import { GetCommits } from 'src/app/actions/commit.action';

@Component({
  selector: 'app-commits',
  templateUrl: './commits.component.html',
  styleUrls: ['./commits.component.scss']
})
export class CommitsComponent implements OnInit {

  @Select(CommitState.getCommits) commits: Observable<Commit[]>

  constructor(
    private store: Store,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.dispatch(new GetCommits(params['owner'], params['repo']));
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import { RepositoryState } from '../../states/repository.state';
import { GetRepositories, SetLoading } from '../../actions/repository.action';
import { Observable, Subscription } from 'rxjs';
import { SearchForm } from '../../models/SearchForm';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Select(RepositoryState.getSearchForm) searchForm: Observable<SearchForm>;
  form: FormGroup;
  sizes = [10, 20, 30, 40, 50, 100];
  timeId;
  private formSubscription: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.formSubscription.add(
      this.searchForm.subscribe(form => {
        this.form.patchValue({
          q: form.q,
          size: form.size
        });
      })
    );
  }

  setText() {
    clearTimeout(this.timeId);
    this.timeId = setTimeout(() => {
      this.store.dispatch(new SetLoading(true));
      this.store.dispatch(new GetRepositories(this.form.value.q, this.form.value.size)).subscribe(() => {
        this.store.dispatch(new SetLoading(false));
      })
    }, 300);
  }

  setSize() {
    console.log(this.form.value.size);
    this.store.dispatch(new SetLoading(true));
    this.store.dispatch(new GetRepositories(this.form.value.q, this.form.value.size)).subscribe(() => {
      this.store.dispatch(new SetLoading(false));
    })
  }

  createForm() {
    this.form = this.fb.group({
      q: [''],
      size: [10]
    });
  }

}

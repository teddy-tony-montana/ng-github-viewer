import { Component, OnInit } from '@angular/core';
import { RepositoryState } from '../../states/repository.state';
import { Select, Store } from '@ngxs/store';
import { Repository } from '../../models/Repository';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Select(RepositoryState.getRepositoryList) repositories: Observable<Repository[]>;

  constructor() { }

  ngOnInit() { }

}

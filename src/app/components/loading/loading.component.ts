import { Component, OnInit } from '@angular/core';
import { RepositoryState } from '../../states/repository.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  @Select(RepositoryState.getLoading) loading: Observable<boolean>;

  constructor() { }

  ngOnInit() {
  }

}

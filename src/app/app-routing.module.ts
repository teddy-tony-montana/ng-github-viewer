import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { CommitsComponent } from './components/commits/commits.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'details/:owner/:repo', component: DetailComponent },
  { path: 'commits/:owner/:repo', component: CommitsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

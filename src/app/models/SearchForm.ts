export interface SearchForm {
  "q": string;
  "size": number;
}
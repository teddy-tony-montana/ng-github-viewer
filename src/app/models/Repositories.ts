import { Repository } from './Repository';

export interface Repositories {
  "total_count": number,
  "incomplete_results": boolean,
  "items": Repository[]
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Repositories } from './../models/Repositories';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(private http: HttpClient) { }

  fetchRepositories(q: string, size: number) {
    return this.http.get<Repositories>(`https://api.github.com/search/repositories?q=${q}&per_page=${size}`);
  }

  fetchRepository(owner: string, repo: string) {
    return this.http.get<any>(`https://api.github.com/repos/${owner}/${repo}`);
  }

  fetchCommits(owner: string, repo: string) {
    return this.http.get<any>(`https://api.github.com/repos/${owner}/${repo}/commits`);
  }

}

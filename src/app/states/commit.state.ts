import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Commit } from '../models/Commit';
import { GetCommits } from '../actions/commit.action';
import { RepositoryService } from '../services/repository.service';
import { tap } from 'rxjs/operators';

export class CommitStateModel {
  commits: Commit[];
}

@State<CommitStateModel>({
  name: 'commits',
  defaults: {
    commits: null
  }
})
export class CommitState {

  constructor(private repositorySerivce: RepositoryService) { }

  @Selector()
  static getCommits(state: CommitStateModel) {
    return state.commits;
  }

  @Action(GetCommits)
  getRepository({ getState, setState }: StateContext<CommitStateModel>, { owner, repo }: GetCommits) {
    return this.repositorySerivce.fetchCommits(owner, repo).pipe(tap((result) => {
      console.log(result);
      const state = getState();
      setState({
        ...state,
        commits: result
      })
    }))
  }

}
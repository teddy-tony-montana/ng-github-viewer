import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Repositories } from '../models/Repositories';
import { Repository } from '../models/Repository';
import { SearchForm } from '../models/SearchForm';
import { GetRepositories, GetRepository, SetSearchForm, SetLoading } from '../actions/repository.action';
import { RepositoryService } from '../services/repository.service';
import { tap } from 'rxjs/operators';

export class RepositoryStateModel {
  repositories: Repository[];
  q: string;
  repository: Repository;
  loading: boolean;
  searchForm: SearchForm;
}

@State<RepositoryStateModel>({
  name: 'repositories',
  defaults: {
    repositories: null,
    q: '',
    repository: null,
    loading: false,
    searchForm: {
      q: null,
      size: 10
    }
  }
})
export class RepositoryState {

  constructor(private repositorySerivce: RepositoryService) { }

  @Selector()
  static getRepositoryList(state: RepositoryStateModel) {
    return state.repositories;
  }

  @Selector()
  static getRepository(state: RepositoryStateModel) {
    return state.repository;
  }

  @Selector()
  static getSearchForm(state: RepositoryStateModel) {
    return state.searchForm;
  }

  @Selector()
  static getLoading(state: RepositoryStateModel) {
    return state.loading;
  }

  @Action(GetRepositories)
  getRepositories({ getState, setState }: StateContext<RepositoryStateModel>, { q, size }: GetRepositories) {
    return this.repositorySerivce.fetchRepositories(q, size).pipe(tap((result) => {
      const state = getState();
      setState({
        ...state,
        repositories: result.items
      })
    }))
  }

  @Action(GetRepository)
  getRepository({ getState, setState }: StateContext<RepositoryStateModel>, { owner, repo }: GetRepository) {
    return this.repositorySerivce.fetchRepository(owner, repo).pipe(tap((result) => {
      console.log(result);
      const state = getState();
      setState({
        ...state,
        repository: result
      })
    }))
  }

  @Action(SetSearchForm)
  setSelectedTodoId({ getState, setState }: StateContext<RepositoryStateModel>, payload: SearchForm) {
    console.log('payload',payload);
    const state = getState();
    setState({
      ...state,
      searchForm: payload
    });
  }

  @Action(SetLoading)
  setLoading({ getState, setState }: StateContext<RepositoryStateModel>, { payload }: SetLoading) {
    const state = getState();
    setState({
      ...state,
      loading: payload
    });
  }

}
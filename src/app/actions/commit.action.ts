import { Commit } from '../models/Commit';

export class GetCommits {
  static readonly type = '[Commits] Get';

  constructor(public owner: string, public repo: string) { }
}
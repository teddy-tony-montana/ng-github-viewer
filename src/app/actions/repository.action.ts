import { Repositories } from '../models/Repositories';
import { SearchForm } from '../models/SearchForm';

export class GetRepositories {
  static readonly type = '[Repositories] Get';

  constructor(public q: string, public size: number) { }
}

export class GetRepository {
  static readonly type = '[Repository] Get';

  constructor(public owner: string, public repo: string) { }
}

export class SetSearchForm {
  static readonly type = '[Search] Set';

  constructor(public payload: SearchForm) { }
}

export class GetLoading {
  static readonly type = '[Loading] Get';

  constructor() { }
}

export class SetLoading {
  static readonly type = '[Loading] Set';

  constructor(public payload: boolean) { }
}